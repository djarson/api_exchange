package com.example.api_exchange.repository;

import com.example.api_exchange.model.entity.ExchangeRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExchangeRateRepository extends JpaRepository<ExchangeRate,Integer> {
    ExchangeRate findByType(String type);
}
