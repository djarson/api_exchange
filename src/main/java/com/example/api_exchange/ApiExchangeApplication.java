package com.example.api_exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiExchangeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiExchangeApplication.class, args);
    }

}
