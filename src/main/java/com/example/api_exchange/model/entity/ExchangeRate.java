package com.example.api_exchange.model.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class ExchangeRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String type;

    @Column(name="_value")
    private double value;

}
